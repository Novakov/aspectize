﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aspectize;

namespace SampleProject.Aspects
{
    public class TraceCallAttribute : MethodBoundaryAspect
    {
        public override void OnEnter(MethodExecutionArgs args)
        {
            Console.ForegroundColor = ConsoleColor.Green;

            Console.WriteLine("Entering: {0}", args.Method.Name);

            Console.ForegroundColor = ConsoleColor.White;
        }

        public override void OnExit(MethodExecutionArgs args)
        {
            Console.ForegroundColor = ConsoleColor.Green;

            Console.WriteLine("Exit: {0}", args.Method.Name);

            Console.ForegroundColor = ConsoleColor.White;
        }
    }
}
