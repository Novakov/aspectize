﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aspectize;

namespace SampleProject
{
    public enum EnumType
    {
        A,
        B
    }
    
    class LogAttribute : MethodBoundaryAspect
    {
        private readonly string text;
        public LogAttribute(string text, EnumType e)
        {
            this.text = text;
        }

        public override void OnEnter(MethodExecutionArgs args)
        {
            //Console.ReadLine();
            Console.WriteLine("[{2}-{0}]OnEnter {1}", text, args.Method.Name, Extra);
        }

        public override void OnExit(MethodExecutionArgs args)
        {
            Console.WriteLine("[{0}]OnExit: {1}", text, args.Method.Name);
        }

        public override void OnError(MethodExecutionArgs args)
        {
            Console.WriteLine("[{0}]Error: {1}", text, args.Exception);
        }

        public string Extra
        {
            get;
            set;
        }

        public int Z
        {
            get;
            set;
        }
    }
}
