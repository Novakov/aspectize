﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aspectize;

namespace SampleProject
{
    public class InterceptCallAttribute : MethodInterceptionAspect
    {
        public override void OnInvoke(MethodInterceptionArgs args)
        {
            Console.WriteLine("Invoking");
            args.Invoker.Invoke(args.Arguments);            
            Console.WriteLine("invoked");
        }
    }
}
