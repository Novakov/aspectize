﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SampleProject
{
    //[Log("class", EnumType.A)]
    class Program
    {
        [Log("method", EnumType.A, Extra = "aaaa", Z = 10)]
        static void Test(string[] args, int i)
        {
            //Console.ReadLine();
            Console.WriteLine("In Main");

            //throw new Exception();

            //Console.ReadLine();
        }

        [Log("method", EnumType.A)]
        static int x()
        {
            return 10;
        }

        //[InterceptCall]
        public static void Call(int i)
        {
            Console.WriteLine("Call({0})", i);
        }

        //[CatchResult]
        public static int CatchResult()
        {
            return 15;
        }

        //[OmitCallAndReturn]
        public static int Call()
        {
            Console.WriteLine("This will never run");

            return 6;
        }

        [Aspects.TraceCall]
        public static void TraceSample()
        {
            Console.WriteLine("inside method");
        }

        [InterceptCall]
        public static int InterceptRef(string input, ref string refParam, out int outParam)
        {
            outParam = 3;
            return 0;
        }

        static void Main()
        {
            //Console.ReadLine();
            //Test(null, 0);

            //Console.WriteLine(x());

            //Call(10);

            //Console.WriteLine(CatchResult());

            //Console.WriteLine(Call());

            //TraceSample();

            Console.ReadLine();

            string refParam = "refInput";
            int outParam;
            int result = InterceptRef("input", ref refParam, out outParam);
            Console.WriteLine("result: {0} ref: {1} out: {2}", result, refParam, outParam);
        }
    }
}
