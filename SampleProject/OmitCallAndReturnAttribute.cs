﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aspectize;

namespace SampleProject
{
    public class OmitCallAndReturnAttribute : MethodInterceptionAspect
    {
        public override void OnInvoke(MethodInterceptionArgs args)
        {
            Console.WriteLine("OnInvoke");

            args.Result = 666;
        }
    }
}
