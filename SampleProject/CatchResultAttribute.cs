﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aspectize;

namespace SampleProject
{
    public class CatchResultAttribute : MethodBoundaryAspect
    {
        public override void OnExit(MethodExecutionArgs args)
        {
            Console.WriteLine("Exiting with result: {0}", args.Result);
        }
    }
}
