﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Reflection;

namespace Aspectize
{
    public abstract class AspectAttribute : Attribute
    {
        private Regex namespaceRegex;
        private Regex typeRegex;
        private Regex memberRegex;

        public string NamespaceFilter
        {
            get;
            set;
        }

        public string TypeFilter
        {
            get;
            set;
        }

        public string MemberFilter
        {
            get;
            set;
        }

        public virtual bool IsValidOnNamespace(string @namespace)
        {
            if(string.IsNullOrWhiteSpace(NamespaceFilter))
            {
                return true;
            }

            if(namespaceRegex == null)
            {
                namespaceRegex = new Regex(NamespaceFilter);
            }

            return namespaceRegex.IsMatch(@namespace);
        }

        public virtual bool IsValidOnType(Type type)
        {
            if(string.IsNullOrWhiteSpace(TypeFilter))
            {
                return IsValidOnNamespace(type.Namespace);
            }

            if(typeRegex == null)
            {
                typeRegex = new Regex(TypeFilter);
            }

            return IsValidOnNamespace(type.Namespace) && typeRegex.IsMatch(type.Name);
        }

        public virtual bool IsValidOnMember(MemberInfo memberInfo)
        {
            if(string.IsNullOrWhiteSpace(MemberFilter))
            {
                return IsValidOnType(memberInfo.DeclaringType);
            }

            if(memberRegex == null)
            {
                memberRegex = new Regex(MemberFilter);
            }

            return IsValidOnType(memberInfo.DeclaringType) && memberRegex.IsMatch(memberInfo.Name);
        }
    }
}
