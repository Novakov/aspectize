﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Aspectize
{
    [AttributeUsage(AttributeTargets.Class)]
    public sealed class AspectScopeAttribute : Attribute
    {        
        public string NamespaceFilter
        {
            get;
            set;
        }

        public string TypeFilter
        {
            get;
            set;
        }

        public string MemberFilter
        {
            get;
            set;
        }
    }
}
