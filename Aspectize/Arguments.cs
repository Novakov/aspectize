﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Aspectize
{
    public sealed class Arguments
    {
        private readonly IDictionary<string, object> arguments;

        public Arguments()
        {
            arguments = new Dictionary<string, object>();
        }

        public object this[string name]
        {
            get
            {
                return arguments[name];
            }
            set
            {
                arguments[name] = value;
            }
        }

        public T Get<T>(string name)
        {
            return (T) arguments[name];
        }

        internal void AddArgument(string name, object value)
        {
            arguments.Add(name, value);
        }

        public void Set<T>(string name, T value)
        {
            arguments[name] = value;
        }
    }
}
