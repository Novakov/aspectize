﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Aspectize
{
    public class MethodInterceptionArgs : BaseMethodExecutionArgs
    {
        public object Result
        {
            get;
            set;
        }

        public Invoker Invoker
        {
            get;
            internal set;
        }

        public T GetResult<T>()
        {
            return (T) Result;                
        }
    }
}
