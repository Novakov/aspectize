﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

namespace Aspectize
{
    public abstract class BaseMethodExecutionArgsBuilder<TBuilder, TArgs>
        where TArgs : BaseMethodExecutionArgs
        where TBuilder : BaseMethodExecutionArgsBuilder<TBuilder, TArgs>
    {
        public TArgs Args
        {
            get;
            protected set;
        }        

        public TBuilder AddArgument(string name, object value)
        {
            Args.Arguments.AddArgument(name, value);

            return (TBuilder) this;
        }

        public TArgs Build()
        {
            return Args;
        }
    }
}
