﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

namespace Aspectize
{
    public class MethodExecutionArgs : BaseMethodExecutionArgs
    {
        public Exception Exception
        {
            get;
            private set;
        }

        public object Result
        {
            get;
            private set;
        }

        public MethodExecutionArgs SetException(Exception e)
        {
            this.Exception = e;
            return this;
        }

        public void SetResult<T>(T result)
        {
            Result = result;
        }
    }
}
