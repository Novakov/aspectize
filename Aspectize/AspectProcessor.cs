﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Aspectize
{
    [AttributeUsage(AttributeTargets.Class, Inherited = true, AllowMultiple = false)]
    public class AspectProcessorAttribute : Attribute
    {
        public AspectProcessorAttribute(string processorName)
        {
            ProcessorName = processorName;
        }

        public string ProcessorName
        {
            get;
            private set;
        }
    }
}
