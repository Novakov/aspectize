﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

namespace Aspectize
{
    public class MethodInterceptionArgsBuilder : BaseMethodExecutionArgsBuilder<MethodInterceptionArgsBuilder, MethodInterceptionArgs>
    {        
        public static MethodInterceptionArgsBuilder ForMethod(MethodBase method, object target)
        {
            return new MethodInterceptionArgsBuilder
            {
                Args = new MethodInterceptionArgs
                {
                    Method = method,
                    Target = target                    
                }
            };
        }
        
        public MethodInterceptionArgsBuilder DefaultReturn<T>()
        {
            Args.Result = default(T);
            return this;
        }

        public MethodInterceptionArgsBuilder Invoker(Invoker invoker)
        {
            Args.Invoker = invoker;

            return this;
        }        
    }
}
