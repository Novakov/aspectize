﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

namespace Aspectize
{
    public class MethodExecutionArgsBuilder : BaseMethodExecutionArgsBuilder<MethodExecutionArgsBuilder, MethodExecutionArgs>
    {
        public static MethodExecutionArgsBuilder ForMethod(MethodBase method, object target)
        {
            return new MethodExecutionArgsBuilder
            {
                Args = new MethodExecutionArgs
                {
                    Method = method,
                    Target = target
                }
            };
        }
    }
}
