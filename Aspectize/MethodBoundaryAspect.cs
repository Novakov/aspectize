﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Aspectize
{    
    [AspectProcessor(AspectProcessors.MethodBoundaryProcessor)]
    public abstract class MethodBoundaryAspect : AspectAttribute
    {
        public virtual void OnEnter(MethodExecutionArgs args)
        {
        }

        public virtual void OnExit(MethodExecutionArgs args)
        {           
        }

        public virtual void OnError(MethodExecutionArgs args)
        {
        }
    }
}
