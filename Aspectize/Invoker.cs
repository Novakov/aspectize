﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Aspectize
{
    public abstract class Invoker
    {
        public abstract object Invoke(Arguments arguments);
    }

    public abstract class Invoker<TDelegate> : Invoker
    {
        public readonly TDelegate @delegate;

        public Invoker(TDelegate @delegate)
        {
            this.@delegate = @delegate;
        }
    }
}
