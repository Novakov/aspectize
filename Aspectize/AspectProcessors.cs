﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Aspectize
{
    public static class AspectProcessors
    {
        public const string MethodBoundaryProcessor = "Aspectize.PostCompiler.Processors.MethodBoundaryProcessor, Aspectize.PostCompiler";
        public const string MethodInterceptionProcessor = "Aspectize.PostCompiler.Processors.MethodInterceptionProcessor, Aspectize.PostCompiler";
    }
}
