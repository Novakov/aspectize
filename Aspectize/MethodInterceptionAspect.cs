﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Aspectize
{
    [AspectProcessor(AspectProcessors.MethodInterceptionProcessor)]
    public class MethodInterceptionAspect : AspectAttribute
    {
        public virtual void OnInvoke(MethodInterceptionArgs args)
        {
        }
    }
}
