using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Aspectize
{
    public abstract class BaseMethodExecutionArgs
    {
        public BaseMethodExecutionArgs()
        {
            Arguments = new Arguments();
        }

        public MethodBase Method
        {
            get;
            internal set;
        }

        public object Target
        {
            get;
            internal set;
        }

        public Arguments Arguments
        {
            get;
            set;
        }
    }
}
