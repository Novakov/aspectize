﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Build.Framework;
using Microsoft.Build.Utilities;
using Aspectize.PostCompiler;

namespace Aspectize.MSBuild
{
    [LoadInSeparateAppDomain]
    public class InjectAspects : AppDomainIsolatedTask
    {
        public InjectAspects()
        {
            GenerateIntermediateResults = false;
        }
        
        public override bool Execute()
        {
            var injector = new Injector(AssemblyPath);

            injector.DebugSymbols = DebugSymbols;
            injector.References = References;
            injector.GenerateIntermediateResults = GenerateIntermediateResults;

            injector.Inject();

            return true;
        }

        [Required]
        public string AssemblyPath
        {
            get;
            set;
        }

        [Required]
        public string[] References
        {
            get;
            set;
        }

        public string DebugSymbols
        {
            get;
            set;
        }

        public bool GenerateIntermediateResults
        {
            get;
            set;
        }
    }
}
