$sdkToolsPath = $(Get-ItemProperty -Path "HKLM:\SOFTWARE\Microsoft\Microsoft SDKs\Windows\v7.0A\WinSDK-NetFx40Tools").InstallationFolder

$dotNetPath = "$($Env:SystemRoot)\Microsoft.NET\Framework\v4.0.30319\"

Set-Alias peverify "$($sdkToolsPath)peverify.exe" -Scope Global
Set-Alias ildasm "$($sdkToolsPath)ildasm.exe" -Scope Global
Set-Alias msbuild "$($dotNetPath)msbuild.exe" -Scope Global
Set-Alias nuget "$(gl)\packages\NuGet.CommandLine.1.6.0\tools\NuGet.exe" -Scope Global
