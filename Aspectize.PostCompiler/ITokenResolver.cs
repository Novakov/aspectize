﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aspectize.PostCompiler.Analyzing;

namespace Aspectize.PostCompiler
{
    public interface ITokenResolver
    {
        object Resolve(TokenReference token);
        T Resolve<T>(TokenReference token)
            where T : class;
    }
}
