﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Aspectize.PostCompiler
{
    public static class EnumerableExtensions
    {
        public static IEnumerable<T> Replace<T>(this IEnumerable<T> @this, Func<T, bool> predicate, Func<T, T> newItem)
        {
            foreach(var item in @this)
            {
                if(predicate(item))
                {
                    yield return newItem(item);
                }
                else
                {
                    yield return item;
                }
            }
        }

        public static IEnumerable<T> Replace<T>(this IEnumerable<T> @this, Func<T, bool> predicate, IEnumerable<Func<T, T>> newItems)
        {
            foreach(var item in @this)
            {
                if(predicate(item))
                {
                    foreach(var newItem in newItems)
                    {
                        yield return newItem(item);
                    }
                }
                else
                {
                    yield return item;
                }
            }
        }
    }
}
