﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Diagnostics;

namespace Aspectize.PostCompiler.Analyzing
{
    public static class Helper
    {
        public const BindingFlags AllVisibility = BindingFlags.Public | BindingFlags.NonPublic;
        public const BindingFlags AllBinding = BindingFlags.Instance | BindingFlags.Static;
        public const BindingFlags AllDeclaredMembers = AllVisibility | AllBinding | BindingFlags.DeclaredOnly;
        
        public static bool IsAspectType(this Type type)
        {
            return typeof(AspectAttribute).IsAssignableFrom(type);
        }

        public static AspectInfo GetAspectInfo(this AttributeWithData attribute)
        {
            Type aspectType = attribute.Attribute.GetType();

            var processor = aspectType.GetAttribute<AspectProcessorAttribute>();
            if(processor == null)
            {
                throw new InvalidOperationException("Missing aspect processor attribute for " + aspectType.FullName);
            }

            return new AspectInfo
            {
                AspectName = aspectType.AssemblyQualifiedName,
                AspectToken = TokenReference.For(aspectType),

                AspectCtorToken = TokenReference.For(attribute.Data.Constructor),
                AspectCtorArguments = attribute.Data.ConstructorArguments.Select(x => x.Value).ToArray(),

                AspectCtorNamedArguments = attribute.Data.NamedArguments.Select(x => Tuple.Create(x.MemberInfo.Name, x.TypedValue.Value)).ToArray(),

                AspectInstance = (AspectAttribute) attribute.Attribute,

                AspectProcessor = processor.ProcessorName
            };
        }

        public static IEnumerable<AspectInfo> AspectInfos(this IEnumerable<AttributeWithData> @this)
        {
            return @this.Where(x => x.Attribute is AspectAttribute).Select(x => x.GetAspectInfo());
        }

        public static IEnumerable<AttributeWithData> GetAttributesWithData(this Assembly @this)
        {
            return Merge(@this.GetCustomAttributes(false), @this.GetCustomAttributesData());
        }

        public static IEnumerable<AttributeWithData> GetAttributesWithData(this Type @this)
        {            
            return Merge(@this.GetCustomAttributes(false), @this.GetCustomAttributesData());
        }

        public static IEnumerable<AttributeWithData> GetAttributesWithData(this MemberInfo @this)
        {
            return Merge(@this.GetCustomAttributes(false), @this.GetCustomAttributesData());
        }

        private static IEnumerable<AttributeWithData> Merge(object[] attributes, IList<CustomAttributeData> data)
        {
            for(int i = 0; i < attributes.Length; i++)
            {
                yield return new AttributeWithData(attributes[i], data[i]);
            }
        }
    }
}
