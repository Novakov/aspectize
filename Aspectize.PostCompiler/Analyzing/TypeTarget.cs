using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspectize.PostCompiler.Analyzing
{
    [Serializable]
    public class TypeTarget : TokenTarget
    {
        public TypeTarget(int token, string name)
            : base(token, name)
        {

        }

        public override Processors.TargetTypes TargetType
        {
            get
            {
                return Processors.TargetTypes.Type;
            }
        }
    }
}
