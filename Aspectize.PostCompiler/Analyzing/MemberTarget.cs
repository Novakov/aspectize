using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Aspectize.PostCompiler.Analyzing
{
    [Serializable]
    public class MemberTarget : TokenTarget
    {
        public MemberTarget(int token, string name, MemberTypes memberType)
            : base(token, name)
        {
            MemberType = memberType;
        }

        public MemberTypes MemberType
        {
            get;
            private set;
        }

        public override string ToString()
        {
            return string.Format("Target: {0} {1} ({2})", MemberType, Name, Token);
        }

        public override Processors.TargetTypes TargetType
        {
            get
            {
                switch(MemberType)
                {                                            
                    case MemberTypes.Constructor:
                        return Processors.TargetTypes.Ctor;
                    case MemberTypes.Method:
                        return Processors.TargetTypes.Method;                    
                    case MemberTypes.Event:
                        return Processors.TargetTypes.Event;
                    case MemberTypes.Field:
                        return Processors.TargetTypes.Field;
                    case MemberTypes.NestedType:
                        return Processors.TargetTypes.Type;
                    case MemberTypes.Property:
                        return Processors.TargetTypes.Property;                    
                    default:
                        throw new ArgumentOutOfRangeException("Unrecognized member type");
                }
            }
        }
    }
}
