﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Text.RegularExpressions;
using System.IO;

namespace Aspectize.PostCompiler.Analyzing
{
    [Serializable]
    public class AssemblyAnalyzer : MarshalByRefObject
    {
        public KeyedList<Target, AspectInfo> Analyze(string assemblyFile)
        {
            AppDomain.CurrentDomain.AssemblyResolve += new ResolveEventHandler(CurrentDomain_AssemblyResolve);

            var aspects = new KeyedList<Target, AspectInfo>();

            var assembly = Assembly.LoadFrom(assemblyFile);

            var assemblyLevel = assembly.GetAttributesWithData().AspectInfos().ToList();
            aspects.Add(new AssemblyTarget(assembly.FullName), assemblyLevel);

            foreach(var item in assembly.GetTypes().Where(x => !typeof(AspectAttribute).IsAssignableFrom(x)))
            {
                aspects.Merge(ProcessType(item, assemblyLevel));
            }

            return aspects;
        }

        Assembly CurrentDomain_AssemblyResolve(object sender, ResolveEventArgs args)
        {
            if(args.Name.Equals(Assembly.GetExecutingAssembly().FullName))
            {
                return Assembly.GetExecutingAssembly();
            }

            var asmName = new AssemblyName(args.Name);

            var path = References.SingleOrDefault(x => Path.GetFileNameWithoutExtension(x).Equals(asmName.Name, StringComparison.CurrentCultureIgnoreCase));
            if(path != null)
            {
                return Assembly.LoadFrom(path);
            }

            return null;
        }

        private KeyedList<Target, AspectInfo> ProcessType(Type type, List<AspectInfo> assemblyLevel)
        {
            var aspects = new KeyedList<Target, AspectInfo>();

            var typeLevel = type.GetAttributesWithData().AspectInfos().Union(assemblyLevel).Where(x => x.AspectInstance.IsValidOnType(type)).ToList();

            aspects.Add(new TypeTarget(type.MetadataToken, type.FullName), typeLevel);

            foreach(var item in type.GetMembers(Helper.AllDeclaredMembers))
            {
                var target = new MemberTarget(item.MetadataToken, item.ToString(), item.MemberType);

                var memberLevel = item.GetAttributesWithData().AspectInfos().Union(typeLevel).Where(x => x.AspectInstance.IsValidOnMember(item));

                aspects.Add(target, memberLevel);
            }

            return aspects;
        }

        public string[] References
        {
            get;
            set;
        }
    }
}
