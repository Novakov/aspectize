﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Aspectize.PostCompiler.Analyzing
{
    [Serializable]
    public abstract class TokenTarget : Target
    {
        public TokenTarget(int token, string name)
        {
            Token = token;
            Name = name;
        }

        public int Token
        {
            get;
            set;
        }
        public string Name
        {
            get;
            set;
        }

        public override string ToString()
        {
            return string.Format("{0}: {1} ({2})", GetType().Name, Name, Token);
        }

        public override int GetHashCode()
        {
            return Token;
        }

        public override bool Equals(object obj)
        {
            var other = obj as TokenTarget;
            if(other == null)
            {
                return false;
            }

            //if(other.GetType() != this.GetType())
            //{
            //    return false;
            //}

            return other.Token == this.Token;
        }
    }
}