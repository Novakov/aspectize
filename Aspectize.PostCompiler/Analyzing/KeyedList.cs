﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace Aspectize.PostCompiler.Analyzing
{
    [Serializable]
    public class KeyedList<TKey, TElement>: Dictionary<TKey, List<TElement>>
    {
        public KeyedList()
        {

        }

        protected KeyedList(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {          
        }

        public void Add(TKey key, TElement element)
        {
            if(ContainsKey(key))
            {
                this[key].Add(element);
            }
            else
            {
                this.Add(key, new List<TElement> { element });
            }
        }

        public void Add(TKey key, IEnumerable<TElement> elements)
        {
            if(ContainsKey(key))
            {
                this[key].AddRange(elements);
            }
            else
            {
                base.Add(key, new List<TElement>(elements));
            }
        }

        public void Merge(KeyedList<TKey, TElement> other)
        {
            foreach(var item in other)
            {
                this.Add(item.Key, elements: item.Value);
            }
        }
    }
}
