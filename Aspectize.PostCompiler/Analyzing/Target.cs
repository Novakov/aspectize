﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aspectize.PostCompiler.Processors;

namespace Aspectize.PostCompiler.Analyzing
{
    [Serializable]
    public abstract class Target
    {
        public abstract override int GetHashCode();
        public abstract override bool Equals(object obj);

        public abstract TargetTypes TargetType
        {
            get;
        }
    }
}
