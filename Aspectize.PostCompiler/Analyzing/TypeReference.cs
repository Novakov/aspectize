﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

namespace Aspectize.PostCompiler.Analyzing
{
    [Serializable]
    public class TokenReference
    {
        public int Token
        {
            get;
            set;
        }

        public string AssemblyName
        {
            get;
            set;
        }

        public static TokenReference For(Type type)
        {
            return new TokenReference
            {
                Token = type.MetadataToken,
                AssemblyName = type.Assembly.FullName
            };
        }

        public static TokenReference For(MemberInfo type)
        {
            return new TokenReference
            {
                Token = type.MetadataToken,
                AssemblyName = type.DeclaringType.Assembly.FullName
            };
        }
    }
}
