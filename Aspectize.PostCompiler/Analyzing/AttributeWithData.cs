﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

namespace Aspectize.PostCompiler.Analyzing
{
    public class AttributeWithData
    {
        public AttributeWithData(object attribute, CustomAttributeData data)
        {
            Attribute = attribute;
            Data = data;
        }
        public CustomAttributeData Data
        {
            get;
            private set;
        }
        public object Attribute
        {
            get;
            private set;
        }
    }
}
