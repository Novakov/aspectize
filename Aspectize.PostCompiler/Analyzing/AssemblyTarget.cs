﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

namespace Aspectize.PostCompiler.Analyzing
{
    [Serializable]
    public class AssemblyTarget : Target
    {
        public AssemblyTarget(string assemblyName)
        {
            Name = assemblyName;
        }

        public string Name
        {
            get;
            private set;
        }

        public override int GetHashCode()
        {
            return Name.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            var other = obj as AssemblyTarget;

            if(other == null)
            {
                return false;
            }

            return this.Name == other.Name;
        }

        public override string ToString()
        {
            return string.Format("Assembly target: {0}", Name);
        }

        public override Processors.TargetTypes TargetType
        {
            get
            {
                return Processors.TargetTypes.Assembly;
            }
        }
    }
}
