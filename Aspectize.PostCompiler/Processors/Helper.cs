﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mono.Cecil.Cil;
using Mono.Cecil;
using Aspectize.PostCompiler.Helpers;

namespace Aspectize.PostCompiler.Processors
{
    public static class Helper
    {
        public static List<Instruction> BuildAspectVariable(AspectInfo aspect, MethodReference aspectCtor, ModuleDefinition module)
        {
            var initBlock = new List<Instruction>();

            // initialize aspect variable
            foreach(var item in aspect.AspectCtorArguments)
            {
                initBlock.Add(InstructionHelper.CreatePush(item.GetType(), item));
            }

            initBlock.Add(Instruction.Create(OpCodes.Newobj, aspectCtor));

            TypeDefinition aspectType = aspectCtor.DeclaringType.Resolve();

            foreach(var item in aspect.AspectCtorNamedArguments)
            {
                initBlock.Add(OpCodes.Dup.Create());
                initBlock.Add(InstructionHelper.CreatePush(item.Item2.GetType(), item.Item2));
                
                var propSet = module.Import(aspectType.AllProperties().Where(x => x.Name == item.Item1).First().SetMethod);

                initBlock.Add(OpCodes.Callvirt.Create(propSet));
            }            

            return initBlock;
        }
    }
}
