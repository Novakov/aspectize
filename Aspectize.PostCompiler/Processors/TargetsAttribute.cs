﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Aspectize.PostCompiler.Processors
{
    public sealed class TargetsAttribute : Attribute
    {
        private readonly TargetTypes targets;
        public TargetsAttribute(TargetTypes targets)
        {
            this.targets = targets;
        }
        public TargetTypes Targets
        {
            get
            {
                return targets;
            }
        }
    }
}
