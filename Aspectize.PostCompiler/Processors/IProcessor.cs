﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mono.Cecil;

namespace Aspectize.PostCompiler.Processors
{
    public interface IProcessor
    {
        ITokenResolver TokenResolver
        {
            set;
        }

        void Process(object target, AspectInfo aspect);
    }
}
