﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mono.Cecil;
using Mono.Cecil.Cil;
using Aspectize.PostCompiler.Helpers;
using System.Reflection;
using Mono.Cecil.Rocks;

namespace Aspectize.PostCompiler.Processors
{
    [Targets(TargetTypes.Method)]
    public class MethodBoundaryProcessor : IProcessor
    {
        #region IProcessor Members

        public ITokenResolver TokenResolver
        {
            get;
            set;
        }

        public void Process(object target, AspectInfo aspect)
        {
            var methodDef = target as MethodDefinition;
            var ilProc = methodDef.Body.GetILProcessor();

            var aspectType = methodDef.Module.Import(TokenResolver.Resolve<TypeDefinition>(aspect.AspectToken));

            var aspectVariable = new VariableDefinition("aspect", aspectType);
            methodDef.Body.Variables.Add(aspectVariable);

            VariableDefinition resultVariable = null;

            var writeLine = methodDef.Module.Import(typeof(Console).GetMethod("WriteLine", new[] { typeof(string) }));

            var methodExecutionArgsVariable = new VariableDefinition("executionArgs", methodDef.Module.Import(typeof(MethodExecutionArgs)));
            methodDef.Body.Variables.Add(methodExecutionArgsVariable);

            var beginBlock = BuildBeginBlock(methodDef, aspect, aspectType, aspectVariable, methodExecutionArgsVariable);

            var finallyBlock = BuildFinallyBlock(methodDef, aspectType, aspectVariable, methodExecutionArgsVariable);

            finallyBlock.Add(OpCodes.Endfinally.Create());
            finallyBlock.Add(OpCodes.Nop.Create());

            var catchBlock = BuildCatchBlock(methodDef, aspectType, aspectVariable, methodExecutionArgsVariable);

            var replaceBlock = new List<Func<Instruction, Instruction>>();
            var returnBlock = new List<Instruction>();

            if(methodDef.ReturnType.MetadataType != MetadataType.Void)
            {
                MethodReference setResult = methodDef.Module.Import(ReflectionHelper.GetMethod<MethodExecutionArgs>(x => x.SetResult<int>(Arg.Of<int>())).GetGenericMethodDefinition());

                var genericSetResult = new GenericInstanceMethod(setResult);
                genericSetResult.GenericArguments.Add(methodDef.Module.Import(methodDef.ReturnType));

                resultVariable = new VariableDefinition("result", methodDef.ReturnType);
                methodDef.Body.Variables.Add(resultVariable);

                replaceBlock.Add(instr => OpCodes.Stloc.Create(resultVariable));

                replaceBlock.Add(instr => OpCodes.Ldloc.Create(methodExecutionArgsVariable));
                replaceBlock.Add(instr => OpCodes.Ldloc.Create(resultVariable));
                replaceBlock.Add(instr => OpCodes.Call.Create(genericSetResult));

                returnBlock.Add(OpCodes.Ldloc.Create(resultVariable));
            }

            returnBlock.Add(OpCodes.Ret.Create());

            replaceBlock.Add(instr => OpCodes.Leave_S.Create(returnBlock.First()));

            var body = methodDef.Body.Instructions.ToList()
                            .Replace(x => IsReturnInstruction(methodDef, x), replaceBlock)
                            ;

            methodDef.Body.Instructions.Clear();

            methodDef.Body.Instructions.Add(beginBlock);

            methodDef.Body.Instructions.Add(body);

            methodDef.Body.Instructions.Add(catchBlock);

            methodDef.Body.Instructions.Add(finallyBlock);

            methodDef.Body.Instructions.Add(returnBlock);

            methodDef.Body.ExceptionHandlers.Add(new ExceptionHandler(ExceptionHandlerType.Catch)
            {
                TryStart = body.First(),
                TryEnd = catchBlock.First(),

                HandlerStart = catchBlock.First(),
                HandlerEnd = finallyBlock.First(),

                CatchType = methodDef.Module.Import(typeof(Exception))
            });


            methodDef.Body.ExceptionHandlers.Add(new ExceptionHandler(ExceptionHandlerType.Finally)
                {
                    TryStart = body.First(),
                    TryEnd = finallyBlock.First(),

                    HandlerStart = finallyBlock.First(),
                    HandlerEnd = finallyBlock.Last()
                });


            methodDef.Body.InitLocals = true;

            methodDef.Body.OptimizeMacros();
        }

        private List<Instruction> BuildCatchBlock(MethodDefinition method, TypeReference aspectType, VariableDefinition aspectVariable, VariableDefinition methodExecutionArgsVariable)
        {
            var block = new List<Instruction>();

            var execeptionVariable = new VariableDefinition("e", method.Module.Import(typeof(Exception)));
            method.Body.Variables.Add(execeptionVariable);

            var onEnterMethod = method.Module.Import(aspectType.Resolve().AllMethods().Where(x => x.Name == "OnError").FirstOrDefault());

            var setExceptionMethod = method.Module.Import(ReflectionHelper.GetMethod<MethodExecutionArgs>(x => x.SetException(null)));

            block.Add(OpCodes.Stloc.Create(execeptionVariable));

            block.Add(OpCodes.Ldloc.Create(aspectVariable));
            block.Add(OpCodes.Ldloc.Create(methodExecutionArgsVariable));
            block.Add(OpCodes.Ldloc.Create(execeptionVariable));
            block.Add(OpCodes.Call.Create(setExceptionMethod));

            block.Add(OpCodes.Callvirt.Create(method.Module.Import(onEnterMethod)));

            block.Add(OpCodes.Rethrow.Create());

            return block;
        }

        private static List<Instruction> BuildFinallyBlock(MethodDefinition method, TypeReference aspectType, VariableDefinition aspectVariable, VariableDefinition executionArgs)
        {
            var block = new List<Instruction>();

            block.Add(OpCodes.Ldloc.Create(aspectVariable));
            block.Add(OpCodes.Ldloc.Create(executionArgs));

            var onEnterMethod = method.Module.Import(aspectType.Resolve().AllMethods().Where(x => x.Name == "OnExit").FirstOrDefault());

            block.Add(OpCodes.Callvirt.Create(method.Module.Import(onEnterMethod)));

            return block;
        }

        private List<Instruction> BuildBeginBlock(MethodDefinition methodDef, AspectInfo aspect, TypeReference aspectType, VariableDefinition aspectVariable, VariableDefinition methodExecutionArgsVariable)
        {
            var beginBlock = new List<Instruction>();

            var aspectCtor =  methodDef.Module.Import(TokenResolver.Resolve<MethodDefinition>(aspect.AspectCtorToken));

            beginBlock.AddRange(Helper.BuildAspectVariable(aspect, aspectCtor, methodDef.Module));

            beginBlock.Add(OpCodes.Dup.Create());
            beginBlock.Add(OpCodes.Stloc.Create(aspectVariable));

            beginBlock.AddRange(BuildMethodExecutionArgs(methodDef));
            beginBlock.Add(OpCodes.Dup.Create());
            beginBlock.Add(OpCodes.Stloc.Create(methodExecutionArgsVariable));

            var onEnterMethod = methodDef.Module.Import(aspectType.Resolve().AllMethods().Where(x => x.Name == "OnEnter").FirstOrDefault());

            beginBlock.Add(OpCodes.Callvirt.Create(methodDef.Module.Import(onEnterMethod)));

            return beginBlock;
        }

        private static List<Instruction> BuildMethodExecutionArgs(MethodDefinition method)
        {
            var getMethodFromHandle = method.Module.Import(ReflectionHelper.GetMethod(() => MethodInfo.GetMethodFromHandle(Arg.Of<RuntimeMethodHandle>())));

            var createBuilder = method.Module.Import(ReflectionHelper.GetMethod(() => MethodExecutionArgsBuilder.ForMethod(Arg.Of<MethodInfo>(), Arg.Of<object>())));

            var addArgument = method.Module.Import(ReflectionHelper.GetMethod<MethodExecutionArgsBuilder>(x => x.AddArgument("a", null)));

            var buildMethod = method.Module.Import(ReflectionHelper.GetMethod<MethodExecutionArgsBuilder>(x => x.Build()));

            var block = new List<Instruction>();

            block.Add(OpCodes.Ldtoken.Create(method));
            block.Add(OpCodes.Call.Create(getMethodFromHandle));

            if(method.IsStatic)
            {
                block.Add(OpCodes.Ldnull.Create());
            }
            else
            {
                block.Add(OpCodes.Ldarg_0.Create());
            }

            block.Add(OpCodes.Call.Create(createBuilder));

            foreach(var item in method.Parameters)
            {
                block.Add(OpCodes.Ldstr.Create(item.Name));

                block.Add(OpCodes.Ldarg.Create(item));

                if(item.ParameterType.IsValueType)
                {
                    block.Add(OpCodes.Box.Create(item.ParameterType));
                }

                block.Add(OpCodes.Call.Create(addArgument));
            }

            block.Add(OpCodes.Call.Create(buildMethod));

            return block;
        }

        private bool IsReturnInstruction(MethodDefinition methodDef, Instruction instruction)
        {
            if(instruction.OpCode == OpCodes.Ret)
            {
                return true;
            }

            if(instruction.OpCode == OpCodes.Leave_S)
            {
                var targetInstr = instruction.Operand as Instruction;
                if(targetInstr != null)
                {
                    return IsReturnInstruction(methodDef, targetInstr);
                }
                else
                {
                    return false;
                }
            }

            return false;
        }
        #endregion
    }
}
