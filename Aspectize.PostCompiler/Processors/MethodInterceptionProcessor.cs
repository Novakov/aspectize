﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mono.Cecil;
using Mono.Cecil.Cil;
using Aspectize.PostCompiler.Helpers;
using System.Reflection;
using Mono.Cecil.Rocks;

namespace Aspectize.PostCompiler.Processors
{
    [Targets(TargetTypes.Method)]
    public class MethodInterceptionProcessor : IProcessor
    {
        #region IProcessor Members

        public ITokenResolver TokenResolver
        {
            get;
            set;
        }

        public void Process(object target, AspectInfo aspect)
        {
            var method = target as MethodDefinition;

            var declaringType = method.DeclaringType;

            var realMethod = CreateRealMethod(method);

            declaringType.Methods.Add(realMethod);

            var aspectCtor = method.Module.Import(TokenResolver.Resolve<MethodDefinition>(aspect.AspectCtorToken));
            var aspectType = method.Module.Import(TokenResolver.Resolve<TypeDefinition>(aspect.AspectToken));

            method.Body.Variables.Clear();
            method.Body.Instructions.Clear();

            //TODO: Resuse existing delegate type
            string delegateName = string.Format("<{0:x}>{1}", method.MetadataToken.ToInt32(), method.Name);

            var delegateType = Delegates.CreateDelegateType(delegateName, method, method.Module);
            method.DeclaringType.NestedTypes.Add(delegateType);


            var invokerType = CreateInvokerType(delegateType, method.DeclaringType, method);

            var aspectVariable = new VariableDefinition("aspect", aspectType);
            method.Body.Variables.Add(aspectVariable);

            var argsVariable = new VariableDefinition("args", method.Module.Import(typeof(MethodInterceptionArgs)));
            method.Body.Variables.Add(argsVariable);

            var delegateVariable = new VariableDefinition("delegate", delegateType);
            method.Body.Variables.Add(delegateVariable);

            var onInvokeMethod = method.Module.Import(aspectType.Resolve().AllMethods().Where(x => x.Name == "OnInvoke").FirstOrDefault());

            if(method.IsStatic)
            {
                method.Body.Instructions.Add(OpCodes.Ldnull.Create());
            }
            else
            {
                method.Body.Instructions.Add(OpCodes.Ldarg_0.Create());
            }

            method.Body.Instructions.Add(OpCodes.Ldftn.Create(realMethod));
            method.Body.Instructions.Add(OpCodes.Newobj.Create(delegateType.Resolve().GetConstructors().Single()));
            method.Body.Instructions.Add(OpCodes.Stloc.Create(delegateVariable));

            method.Body.Instructions.Add(Helper.BuildAspectVariable(aspect, aspectCtor, method.Module));
            method.Body.Instructions.Add(OpCodes.Stloc.Create(aspectVariable));

            method.Body.Instructions.Add(BuildMethodExecutionArgs(method, delegateVariable, invokerType));
            method.Body.Instructions.Add(OpCodes.Stloc.Create(argsVariable));

            method.Body.Instructions.Add(OpCodes.Ldloc.Create(aspectVariable));
            method.Body.Instructions.Add(OpCodes.Ldloc.Create(argsVariable));
            method.Body.Instructions.Add(OpCodes.Callvirt.Create(method.Module.Import(onInvokeMethod)));

            var arguments = method.Module.Import(typeof(MethodInterceptionArgs).GetProperty("Arguments").GetGetMethod());
            var argGet = method.Module.Import(typeof(Arguments).GetMethod("Get"));

            foreach(var item in method.Parameters.Where(x => x.ParameterType.IsByReference))
            {
                method.Body.Instructions.Add(OpCodes.Ldarg.Create(item));
                method.Body.Instructions.Add(OpCodes.Ldloc.Create(argsVariable));

                method.Body.Instructions.Add(OpCodes.Call.Create(arguments));

                method.Body.Instructions.Add(OpCodes.Ldstr.Create(item.Name));

                var m = new GenericInstanceMethod(argGet);
                m.GenericArguments.Add(item.ParameterType.GetElementType());

                method.Body.Instructions.Add(OpCodes.Call.Create(m));

                method.Body.Instructions.Add(InstructionHelper.CreateStoreIndirect(item.ParameterType.GetElementType()));
            }

            if(method.ReturnType.MetadataType != MetadataType.Void)
            {
                var getResult = method.Module.Import(ReflectionHelper.GetMethod<MethodInterceptionArgs>(x => x.GetResult<int>()).GetGenericMethodDefinition());

                var genericGetResult = new GenericInstanceMethod(getResult);
                genericGetResult.GenericArguments.Add(method.ReturnType);

                method.Body.Instructions.Add(OpCodes.Ldloc.Create(argsVariable));
                method.Body.Instructions.Add(OpCodes.Call.Create(genericGetResult));
            }

            method.Body.Instructions.Add(OpCodes.Ret.Create());

            method.Body.InitLocals = true;

            method.Body.OptimizeMacros();
        }

        private TypeReference CreateInvokerType(TypeReference delegateType, TypeDefinition container, MethodDefinition targetMethod)
        {
            //TODO: reuse existing type

            var typeName = string.Format("<{0:x}>{1}", delegateType.MetadataToken.ToInt32(), delegateType.Name);
            var baseType = delegateType.Module.Import(typeof(Invoker<>)).MakeGenericInstanceType(delegateType);

            var type = new TypeDefinition("", typeName, Mono.Cecil.TypeAttributes.Sealed | Mono.Cecil.TypeAttributes.NestedPrivate, baseType);

            container.NestedTypes.Add(type);

            var ctor = new MethodDefinition(".ctor", Mono.Cecil.MethodAttributes.RTSpecialName | Mono.Cecil.MethodAttributes.SpecialName | Mono.Cecil.MethodAttributes.Public | Mono.Cecil.MethodAttributes.HideBySig, container.Module.Import(typeof(void)));

            ctor.Parameters.Add(new ParameterDefinition("delegate", Mono.Cecil.ParameterAttributes.None, container.Module.Import(delegateType)));

            MethodReference baseCtor = container.Module.Import(baseType.Resolve().GetConstructors().Single());
            baseCtor.DeclaringType = container.Module.Import(baseType);
            //baseCtor.Parameters.First().ParameterType = container.Module.Import(delegateType);

            ctor.Body.Instructions.Add(OpCodes.Ldarg_0.Create());
            ctor.Body.Instructions.Add(OpCodes.Ldarg_1.Create());
            ctor.Body.Instructions.Add(OpCodes.Call.Create(baseCtor));
            ctor.Body.Instructions.Add(OpCodes.Ret.Create());

            type.Methods.Add(ctor);

            var baseInvoke = baseType.Resolve().AllMethods().Where(x => x.Name == "Invoke").Single();
            var invoke = new MethodDefinition("Invoke", Mono.Cecil.MethodAttributes.Public | Mono.Cecil.MethodAttributes.Virtual, baseInvoke.ReturnType);
            invoke.Parameters.Add(new ParameterDefinition("arguments", Mono.Cecil.ParameterAttributes.None, container.Module.Import(typeof(Arguments))));

            invoke.Body.InitLocals = true;


            var argGet = container.Module.Import(typeof(Arguments).GetMethod("Get"));
            var argSet = container.Module.Import(typeof(Arguments).GetMethod("Set"));

            Dictionary<string, VariableDefinition> refParameterVariables = new Dictionary<string, VariableDefinition>();

            foreach(var item in targetMethod.Parameters.Where(x => x.ParameterType.IsByReference))
            {
                var variable = new VariableDefinition(container.Module.Import(item.ParameterType.GetElementType()));
                invoke.Body.Variables.Add(variable);

                invoke.Body.Instructions.Add(OpCodes.Ldarg_1.Create());
                invoke.Body.Instructions.Add(OpCodes.Ldstr.Create(item.Name));

                var m = new GenericInstanceMethod(argGet);
                m.GenericArguments.Add(item.ParameterType.GetElementType());

                invoke.Body.Instructions.Add(OpCodes.Call.Create(m));
                invoke.Body.Instructions.Add(OpCodes.Stloc.Create(variable));

                refParameterVariables[item.Name] = variable;
            }


            FieldReference delegateField = container.Module.Import(baseType.Resolve().Fields.Where(x => x.Name == "delegate").Single());
            delegateField.DeclaringType = container.Module.Import(baseType);

            invoke.Body.Instructions.Add(OpCodes.Ldarg_0.Create());
            invoke.Body.Instructions.Add(OpCodes.Ldfld.Create(container.Module.Import(delegateField)));

            foreach(var item in targetMethod.Parameters)
            {
                if(item.ParameterType.IsByReference)
                {
                    invoke.Body.Instructions.Add(OpCodes.Ldloca.Create(refParameterVariables[item.Name]));
                }
                else
                {
                    invoke.Body.Instructions.Add(OpCodes.Ldarg_1.Create());
                    invoke.Body.Instructions.Add(OpCodes.Ldstr.Create(item.Name));

                    var m = new GenericInstanceMethod(argGet);
                    m.GenericArguments.Add(item.ParameterType);

                    invoke.Body.Instructions.Add(OpCodes.Callvirt.Create(m));
                }
            }

            var delegateInvoke = delegateType.Resolve().Methods.Where(x => x.Name == "Invoke").Single();

            invoke.Body.Instructions.Add(OpCodes.Call.Create(delegateInvoke));


            VariableDefinition result = new VariableDefinition(container.Module.Import(typeof(object)));
            invoke.Body.Variables.Add(result);

            if(targetMethod.ReturnType.MetadataType == MetadataType.Void)
            {
                invoke.Body.Instructions.Add(OpCodes.Ldnull.Create());
            }
            else
            {
                if(targetMethod.ReturnType.IsValueType)
                {
                    invoke.Body.Instructions.Add(OpCodes.Box.Create(targetMethod.ReturnType));
                }

                invoke.Body.Instructions.Add(OpCodes.Stloc.Create(result));
            }

            foreach(var item in refParameterVariables)
            {
                invoke.Body.Instructions.Add(OpCodes.Ldarg_1.Create());
                invoke.Body.Instructions.Add(OpCodes.Ldstr.Create(item.Key));
                invoke.Body.Instructions.Add(OpCodes.Ldloc.Create(item.Value));

                var m = new GenericInstanceMethod(argSet);
                m.GenericArguments.Add(item.Value.VariableType);

                invoke.Body.Instructions.Add(OpCodes.Callvirt.Create(m));
            }

            if(targetMethod.ReturnType.MetadataType != MetadataType.Void)
            {
                invoke.Body.Instructions.Add(OpCodes.Ldloc.Create(result));
            }

            invoke.Body.Instructions.Add(OpCodes.Ret.Create());

            type.Methods.Add(invoke);

            return type;
        }

        #endregion

        private MethodDefinition CreateRealMethod(MethodDefinition method)
        {
            var newMethodName = string.Format("<{2:x}>{0}${1:x}", method.Name, method.MetadataToken.ToInt32(), DateTime.Now.Ticks);

            var newMethodAttributes = ( method.Attributes & ~Mono.Cecil.MethodAttributes.Public ) | Mono.Cecil.MethodAttributes.Private;

            var newMethod = new MethodDefinition(newMethodName, newMethodAttributes, method.ReturnType);
            newMethod.Parameters.Add(method.Parameters);

            newMethod.Body.InitLocals = method.Body.InitLocals;

            newMethod.Body.Variables.Add(method.Body.Variables);

            newMethod.Body.Instructions.Add(method.Body.Instructions);

            return newMethod;
        }

        private static List<Instruction> BuildMethodExecutionArgs(MethodDefinition method, VariableDefinition delegateVariable, TypeReference invokerType)
        {
            var getMethodFromHandle = method.Module.Import(ReflectionHelper.GetMethod(() => MethodInfo.GetMethodFromHandle(Arg.Of<RuntimeMethodHandle>())));

            var createBuilder = method.Module.Import(ReflectionHelper.GetMethod(() => MethodInterceptionArgsBuilder.ForMethod(Arg.Of<MethodInfo>(), Arg.Of<object>())));

            var addArgument = method.Module.Import(ReflectionHelper.GetMethod<MethodInterceptionArgsBuilder>(x => x.AddArgument("a", null)));

            var invokerMethod = method.Module.Import(ReflectionHelper.GetMethod<MethodInterceptionArgsBuilder>(x => x.Invoker(Arg.Of<Invoker>())));

            var defaultReturn = method.Module.Import(ReflectionHelper.GetMethod<MethodInterceptionArgsBuilder>(x => x.DefaultReturn<int>()).GetGenericMethodDefinition());

            var buildMethod = method.Module.Import(ReflectionHelper.GetMethod<MethodInterceptionArgsBuilder>(x => x.Build()));

            var block = new List<Instruction>();

            block.Add(OpCodes.Ldtoken.Create(method));
            block.Add(OpCodes.Call.Create(getMethodFromHandle));

            if(method.IsStatic)
            {
                block.Add(OpCodes.Ldnull.Create());
            }
            else
            {
                block.Add(OpCodes.Ldarg_0.Create());
            }

            block.Add(OpCodes.Call.Create(createBuilder));

            foreach(var item in method.Parameters)
            {
                block.Add(OpCodes.Ldstr.Create(item.Name));

                block.Add(OpCodes.Ldarg.Create(item));

                var realType = item.ParameterType;

                if(item.ParameterType.IsByReference)
                {
                    realType = item.ParameterType.GetElementType();
                    block.Add(InstructionHelper.CreateLoadIndirect(realType));
                }

                if(realType.IsValueType)
                {
                    block.Add(OpCodes.Box.Create(realType));
                }

                block.Add(OpCodes.Call.Create(addArgument));
            }

            block.Add(OpCodes.Ldloc.Create(delegateVariable));
            block.Add(OpCodes.Newobj.Create(invokerType.Resolve().GetConstructors().Single()));
            block.Add(OpCodes.Call.Create(invokerMethod));

            if(method.ReturnType.MetadataType != MetadataType.Void)
            {
                var genericDefaultReturn = new GenericInstanceMethod(defaultReturn);
                genericDefaultReturn.GenericArguments.Add(method.Module.Import(method.ReturnType));

                block.Add(OpCodes.Call.Create(genericDefaultReturn));
            }

            block.Add(OpCodes.Call.Create(buildMethod));

            return block;
        }
    }
}
