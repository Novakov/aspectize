using System;
using System.Collections.Generic;
using System.Linq;

namespace Aspectize.PostCompiler.Processors
{
    [Flags]
    public enum TargetTypes
    {        
        Assembly = 1,
        Type = 2,
        Ctor = 4,
        Method = 8,
        Property = 16,
        Event = 32,
        Field = 64,

        Members = Method | Property | Event | Field
    }
}
