﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mono.Collections.Generic;

namespace Aspectize.PostCompiler
{
    static class CollectionExtensions
    {
        public static void Insert<T>(this Collection<T> @this, int index, IEnumerable<T> items)
        {
            foreach(var item in items.Reverse())
            {
                @this.Insert(0, item);
            }
        }

        public static void Add<T>(this Collection<T> @this, IEnumerable<T> items)
        {
            foreach(var item in items)
            {
                @this.Add(item);
            }
        }
    }
}
