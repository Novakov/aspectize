﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Aspectize.PostCompiler.Helpers
{
    public static class Arg
    {
        public static T Of<T>()
        {
            throw new InvalidOperationException("usable only in expressions");
        }
    }
}
