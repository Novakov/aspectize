﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mono.Cecil;

namespace Aspectize.PostCompiler.Helpers
{
    public static class TypeDefinitionExtensions
    {
        public static IEnumerable<MethodDefinition> AllMethods(this TypeDefinition @this)
        {
            var currentType = @this;
            while(currentType != null)
            {
                foreach(var item in currentType.Methods)
                {
                    yield return item;
                }

                TypeReference baseType = currentType.BaseType;

                currentType = baseType != null ? baseType.Resolve() : null;
            }
        }

        public static IEnumerable<PropertyDefinition> AllProperties(this TypeDefinition @this)
        {
            var currentType = @this;
            while(currentType != null)
            {
                foreach(var item in currentType.Properties)
                {
                    yield return item;
                }

                TypeReference baseType = currentType.BaseType;

                currentType = baseType != null ? baseType.Resolve() : null;
            }
        }
    }
}
