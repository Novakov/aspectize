﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Linq.Expressions;

namespace Aspectize.PostCompiler.Helpers
{
    public static class ReflectionHelper
    {
        public static MethodInfo GetMethod(Expression<Action> method)
        {
            var methodCall = method.Body as MethodCallExpression;
            if(methodCall == null)
            {
                throw new ArgumentException("Lambda must point to method");
            }

            return methodCall.Method;
        }

        public static MethodInfo GetMethod<T>(Expression<Action<T>> method)
        {
            var methodCall = method.Body as MethodCallExpression;
            if(methodCall == null)
            {
                throw new ArgumentException("Lambda must point to method");
            }

            return methodCall.Method;
        }        
    }
}
