﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mono.Cecil;

namespace Aspectize.PostCompiler.Helpers
{
    public static class Delegates
    {
        private const MethodAttributes methodAttrs = Mono.Cecil.MethodAttributes.Public | Mono.Cecil.MethodAttributes.HideBySig | Mono.Cecil.MethodAttributes.NewSlot | Mono.Cecil.MethodAttributes.Virtual;

        public static TypeDefinition CreateDelegateType(string delegateName, MethodDefinition method, ModuleDefinition module)
        {
            var delegateType = new TypeDefinition("", delegateName, Mono.Cecil.TypeAttributes.NestedPrivate | Mono.Cecil.TypeAttributes.Sealed, method.Module.Import(typeof(MulticastDelegate)));

            var ctor = BuildCtor(module);

            delegateType.Methods.Add(ctor);

            delegateType.Methods.Add(BuildBeginInvoke(method, module));

            delegateType.Methods.Add(BuildEndInvoke(method, module));

            delegateType.Methods.Add(BuildInvoke(method));

            return delegateType;
        }

        private static MethodDefinition BuildInvoke(MethodDefinition method)
        {
            var invoke = new MethodDefinition("Invoke", methodAttrs, method.ReturnType);
            invoke.ImplAttributes = Mono.Cecil.MethodImplAttributes.Runtime;

            foreach(var item in method.Parameters)
            {
                invoke.Parameters.Add(item);
            }
            return invoke;
        }

        private static MethodDefinition BuildEndInvoke(MethodDefinition method, ModuleDefinition module)
        {
            var endInvoke = new MethodDefinition("EndInvoke", methodAttrs, method.ReturnType);
            endInvoke.ImplAttributes = Mono.Cecil.MethodImplAttributes.Runtime;            

            foreach(var item in method.Parameters.Where(x => x.ParameterType.IsByReference))
            {
                endInvoke.Parameters.Add(item);
            }

            endInvoke.Parameters.Add(new ParameterDefinition(module.Import(typeof(IAsyncResult))));

            return endInvoke;
        }

        private static MethodDefinition BuildBeginInvoke(MethodDefinition method, ModuleDefinition module)
        {
            var beginInvoke = new MethodDefinition("BeginInvoke", methodAttrs, method.Module.Import(typeof(IAsyncResult)));
            beginInvoke.ImplAttributes = Mono.Cecil.MethodImplAttributes.Runtime;                        

            foreach(var item in method.Parameters)
            {
                beginInvoke.Parameters.Add(item);
            }            

            beginInvoke.Parameters.Add(new ParameterDefinition(module.Import(typeof(AsyncCallback))));
            beginInvoke.Parameters.Add(new ParameterDefinition(module.Import(typeof(object))));
            return beginInvoke;
        }

        private static MethodDefinition BuildCtor(ModuleDefinition module)
        {
            var ctor = new MethodDefinition(".ctor",
                            Mono.Cecil.MethodAttributes.RTSpecialName | Mono.Cecil.MethodAttributes.SpecialName | Mono.Cecil.MethodAttributes.Public | Mono.Cecil.MethodAttributes.HideBySig,
                            module.Import(typeof(void))
                          );

            ctor.Parameters.Add(new ParameterDefinition(module.Import(typeof(object))));
            ctor.Parameters.Add(new ParameterDefinition(module.Import(typeof(IntPtr))));
            ctor.ImplAttributes = Mono.Cecil.MethodImplAttributes.Runtime;
            return ctor;
        }
    }
}
