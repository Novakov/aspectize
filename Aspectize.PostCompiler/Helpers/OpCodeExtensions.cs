﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mono.Cecil.Cil;
using Mono.Cecil;

namespace Aspectize.PostCompiler.Helpers
{
    public static class OpCodeExtensions
    {
        public static Instruction Create(this OpCode @this)
        {            
            return Instruction.Create(@this);
        }

        public static Instruction Create(this OpCode @this, TypeReference arg)
        {
            return Instruction.Create(@this, arg);
        }

        public static Instruction Create(this OpCode @this, CallSite arg)
        {
            return Instruction.Create(@this, arg);
        }

        public static Instruction Create(this OpCode @this, MethodReference arg)
        {
            return Instruction.Create(@this, arg);
        }

        public static Instruction Create(this OpCode @this, FieldReference arg)
        {
            return Instruction.Create(@this, arg);
        }

        public static Instruction Create(this OpCode @this, String arg)
        {
            return Instruction.Create(@this, arg);
        }

        public static Instruction Create(this OpCode @this, SByte arg)
        {
            return Instruction.Create(@this, arg);
        }

        public static Instruction Create(this OpCode @this, Byte arg)
        {
            return Instruction.Create(@this, arg);
        }

        public static Instruction Create(this OpCode @this, Int32 arg)
        {
            return Instruction.Create(@this, arg);
        }

        public static Instruction Create(this OpCode @this, Int64 arg)
        {
            return Instruction.Create(@this, arg);
        }

        public static Instruction Create(this OpCode @this, Single arg)
        {
            return Instruction.Create(@this, arg);
        }

        public static Instruction Create(this OpCode @this, Double arg)
        {
            return Instruction.Create(@this, arg);
        }

        public static Instruction Create(this OpCode @this, Instruction arg)
        {
            return Instruction.Create(@this, arg);
        }

        public static Instruction Create(this OpCode @this, Instruction[] arg)
        {
            return Instruction.Create(@this, arg);
        }

        public static Instruction Create(this OpCode @this, VariableDefinition arg)
        {
            return Instruction.Create(@this, arg);
        }

        public static Instruction Create(this OpCode @this, ParameterDefinition arg)
        {
            return Instruction.Create(@this, arg);
        }
    }
}
