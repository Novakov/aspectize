﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Mono.Cecil.Cil;
using Mono.Cecil;

namespace Aspectize.PostCompiler.Helpers
{
    static public class InstructionHelper
    {
        public static Instruction CreatePush(Type type, object value)
        {
            switch(Type.GetTypeCode(type))
            {
                case TypeCode.Boolean:
                    var b= (bool)value;
                    return Instruction.Create(b ? OpCodes.Ldc_I4_1 : OpCodes.Ldc_I4_0);
                case TypeCode.SByte:
                case TypeCode.Byte:
                    return Instruction.Create(OpCodes.Ldc_I4_S, (byte) value);
                case TypeCode.UInt16:
                case TypeCode.Int16:
                    return Instruction.Create(OpCodes.Ldc_I4, (Int16) value);
                case TypeCode.UInt32:
                case TypeCode.Int32:
                    return Instruction.Create(OpCodes.Ldc_I4, (Int32) value);
                case TypeCode.UInt64:
                case TypeCode.Int64:
                    return Instruction.Create(OpCodes.Ldc_I8, (Int64) value);              
               
                case TypeCode.String:
                    return Instruction.Create(OpCodes.Ldstr, (string) value);
                
                default:
                    throw new InvalidOperationException("Invalid type");
            }
        }

        public static Instruction CreateLoadIndirect(TypeReference type)
        {
            if(!type.IsValueType)
            {
                return OpCodes.Ldind_Ref.Create();
            }

            var typeSystem = type.Module.TypeSystem;

            if(type == typeSystem.Int16)
            {
                return OpCodes.Ldind_I2.Create();
            }

            if(type == typeSystem.Int32)
            {
                return OpCodes.Ldind_I4.Create();
            }

            if(type == typeSystem.Int64)
            {
                return OpCodes.Ldind_I8.Create();
            }

            if(type == typeSystem.Single)
            {
                return OpCodes.Ldind_R4.Create();
            }

            if(type == typeSystem.Double)
            {
                return OpCodes.Ldind_R8.Create();
            }

            if(type == typeSystem.UInt16)
            {
                return OpCodes.Ldind_U2.Create();
            }

            if(type == typeSystem.UInt32)
            {
                return OpCodes.Ldind_U4.Create();
            }            

            return null;
        }

        public static Instruction CreateStoreIndirect(TypeReference type)
        {
            if(!type.IsValueType)
            {
                return OpCodes.Stind_Ref.Create();
            }

            var typeSystem = type.Module.TypeSystem;

            if(type == typeSystem.Int16)
            {
                return OpCodes.Stind_I2.Create();
            }

            if(type == typeSystem.Int32)
            {
                return OpCodes.Stind_I4.Create();
            }

            if(type == typeSystem.Int64)
            {
                return OpCodes.Stind_I8.Create();
            }

            if(type == typeSystem.Single)
            {
                return OpCodes.Stind_R4.Create();
            }

            if(type == typeSystem.Double)
            {
                return OpCodes.Stind_R8.Create();
            }            

            return null;
        }
    }
}
