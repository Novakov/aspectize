﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aspectize.PostCompiler.Processors;

namespace Aspectize.PostCompiler
{
    class ProcessorInfo
    {
        public ProcessorInfo(Type processorType, ITokenResolver tokenResovler)
        {
            this.tokenResovler = tokenResovler;
            ProcessorType = processorType;

            Targets = processorType.GetAttribute<TargetsAttribute>().Targets;
        }

        private IProcessor processor = null;
        private readonly ITokenResolver tokenResovler;
        
        public IProcessor Processor
        {
            get
            {
                if(processor == null)
                {
                    processor = CreateProcessor();
                }

                return processor;
            }
        }

        public Type ProcessorType
        {
            get;
            set;
        }

        public TargetTypes Targets
        {
            get;
            private set;
        }

        public IProcessor CreateProcessor()
        {
            IProcessor processor = (IProcessor) Activator.CreateInstance(ProcessorType);

            processor.TokenResolver = tokenResovler;

            return processor;
        }
    }
}
