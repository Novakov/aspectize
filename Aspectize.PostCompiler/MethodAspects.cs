﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Aspectize.PostCompiler
{
    [Serializable]
    public class MethodAspects
    {
        public string Method
        {
            get;
            set;
        }
        public int MethodToken
        {
            get;
            set;
        }
        public AspectInfo[] Aspects
        {
            get;
            set;
        }
    }
}
