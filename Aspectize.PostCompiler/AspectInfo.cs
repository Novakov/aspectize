﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aspectize.PostCompiler.Analyzing;

namespace Aspectize.PostCompiler
{
    [Serializable]
    public class AspectInfo
    {
        public string AspectName
        {
            get;
            set;
        }

        public string AspectProcessor
        {
            get;
            set;
        }

        public TokenReference AspectToken
        {
            get;
            set;
        }

        public TokenReference AspectCtorToken
        {
            get;
            set;
        }

        public Tuple<string, object>[] AspectCtorNamedArguments
        {
            get;
            set;
        }

        //public string NamespaceFilter
        //{
        //    get;
        //    set;
        //}

        //public string TypeFilter
        //{
        //    get;
        //    set;
        //}

        //public string MemberFilter
        //{
        //    get;
        //    set;
        //}

        public object[] AspectCtorArguments
        {
            get;
            set;
        }

        [NonSerialized]
        private AspectAttribute aspectInstance;
        internal AspectAttribute AspectInstance
        {
            get
            {
                return aspectInstance;
            }
            set
            {
                aspectInstance = value;
            }
        }
        public override string ToString()
        {
            var sb = new StringBuilder();

            sb.AppendFormat("{0}(", AspectName.Split(',').First());

            foreach(var item in AspectCtorArguments)
            {
                sb.AppendFormat("{0}, ", item);
            }

            sb.Append(")");

            return sb.ToString();
        }
    }
}
