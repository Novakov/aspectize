﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using Mono.Cecil;
using System.IO;
using Aspectize.PostCompiler.Processors;
using Mono.Cecil.Cil;
using Mono.Cecil.Pdb;
using Aspectize.PostCompiler.Analyzing;

namespace Aspectize.PostCompiler
{
    public class Injector
    {
        private KeyedList<Target, AspectInfo> aspects;

        public Injector(string inputAssembly)
        {
            InputAssembly = inputAssembly;
        }

        public string InputAssembly
        {
            get;
            private set;
        }

        public string DebugSymbols
        {
            get;
            set;
        }

        public string[] References
        {
            get;
            set;
        }

        public bool GenerateIntermediateResults
        {
            get;
            set;
        }
        public void Inject()
        {
            AnalyzeAssembly();

            ExpandAspects();
        }

        private void AnalyzeAssembly()
        {
            AppDomain.CurrentDomain.AssemblyResolve += new ResolveEventHandler(CurrentDomain_AssemblyResolve);

            var setup = new AppDomainSetup();
            setup.ApplicationBase += Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            setup.PrivateBinPath = Path.GetDirectoryName(InputAssembly);

            var analyzeDomain = AppDomain.CreateDomain("Analyze Domain", AppDomain.CurrentDomain.Evidence, setup);
            var analyzer = (AssemblyAnalyzer) analyzeDomain.CreateInstanceAndUnwrap(typeof(AssemblyAnalyzer).Assembly.FullName, typeof(AssemblyAnalyzer).FullName);

            analyzer.References = References;

            try
            {
                aspects = analyzer.Analyze(InputAssembly);
            }
            finally
            {
                AppDomain.Unload(analyzeDomain);
            }
        }

        private void ExpandAspects()
        {
            var assemblyResolver = new AssemblyResolver();
            assemblyResolver.RemoveSearchDirectory("bin");
            assemblyResolver.RemoveSearchDirectory(".");
            foreach(var item in this.References.Select(x => Path.GetDirectoryName(x).ToLower()).Distinct())
            {
                assemblyResolver.AddSearchDirectory(item);
            }            

            var tokenResolver = new CecilTokenResolver(assemblyResolver);

            var assembly = AssemblyDefinition.ReadAssembly(InputAssembly, new ReaderParameters
            {
                ReadSymbols = true,
                SymbolReaderProvider = GetSymbolReader(),
                AssemblyResolver = assemblyResolver
            });

            assemblyResolver.RegisterAssembly(assembly);

            var processors = new Dictionary<string, ProcessorInfo>();

            var basePath = Path.GetDirectoryName(InputAssembly);
            var baseName = Path.GetFileNameWithoutExtension(InputAssembly);
            var ext = Path.GetExtension(InputAssembly);

            var intermediateDir = Path.Combine(basePath, "Intermediate");

            if(!Directory.Exists(intermediateDir))
            {
                Directory.CreateDirectory(intermediateDir);
            }

            int i = 0;

            using(var sw = new StreamWriter(Path.Combine(basePath, "InjectAspect.log"), false))
            {
                foreach(var target in aspects)
                {
                    foreach(var aspect in target.Value)
                    {
                        if(!processors.ContainsKey(aspect.AspectProcessor))
                        {
                            processors[aspect.AspectProcessor] = new ProcessorInfo(Type.GetType(aspect.AspectProcessor), tokenResolver);
                        }

                        ProcessorInfo processorInfo = processors[aspect.AspectProcessor];
                        if(processorInfo.Targets.HasFlag(target.Key.TargetType))
                        {
                            processorInfo.Processor.Process(ResolveTarget(target.Key, assembly), aspect);

                            var intermediateName = string.Format("{0}_{1}{2}", baseName, i, ext);

                            if(GenerateIntermediateResults)
                            {
                                assembly.Write(Path.Combine(intermediateDir, intermediateName), new WriterParameters
                                {
                                    WriteSymbols = true,
                                    SymbolWriterProvider = GetSymbolWriterProvider()
                                });
                            }

                            sw.WriteLine("Step {0}: {1} Aspect: {2}", i, target, aspect);

                            i++;
                        }
                    }
                }
            }

            assembly.Write(InputAssembly, new WriterParameters
            {
                WriteSymbols = true,
                SymbolWriterProvider = GetSymbolWriterProvider()
            });
        }

        private object ResolveTarget(Target target, AssemblyDefinition assembly)
        {
            switch(target.TargetType)
            {
                case TargetTypes.Assembly:
                    return assembly;
                case TargetTypes.Type:                    
                case TargetTypes.Method:                    
                case TargetTypes.Property:                    
                case TargetTypes.Event:                    
                case TargetTypes.Field:                    
                case TargetTypes.Members:
                    return assembly.MainModule.LookupToken(( target as TokenTarget ).Token);
                default:
                    throw new ArgumentOutOfRangeException("Unrecognized target type");
            }
        }

        private ISymbolWriterProvider GetSymbolWriterProvider()
        {
            if(DebugSymbols != null)
            {
                return new PdbWriterProvider();
            }
            else
            {
                return null;
            }
        }

        private ISymbolReaderProvider GetSymbolReader()
        {
            if(DebugSymbols != null)
            {
                return new PdbReaderProvider();
            }
            else
            {
                return null;
            }
        }

        Assembly CurrentDomain_AssemblyResolve(object sender, ResolveEventArgs args)
        {
            if(args.Name.Equals(Assembly.GetExecutingAssembly().FullName))
            {
                return Assembly.GetExecutingAssembly();
            }

            var asmName = new AssemblyName(args.Name);
            
            var path = References.SingleOrDefault(x => Path.GetFileNameWithoutExtension(x).Equals(asmName.Name, StringComparison.CurrentCultureIgnoreCase));
            if(path != null)
            {
                return Assembly.LoadFrom(path);
            }

            return null;
        }


    }
}
