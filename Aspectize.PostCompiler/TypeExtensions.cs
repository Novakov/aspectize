﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Aspectize.PostCompiler
{
    static class TypeExtensions
    {
        public static T GetAttribute<T>(this Type @this, bool inherit = true)
            where T : Attribute
        {
            return @this.GetCustomAttributes(typeof(T), inherit).OfType<T>().SingleOrDefault();
        }
    }
}
