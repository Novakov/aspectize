﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Aspectize.PostCompiler.Analyzing;
using Mono.Cecil;

namespace Aspectize.PostCompiler
{
    class CecilTokenResolver : ITokenResolver
    {
        private readonly IAssemblyResolver assemblyResolver;
        public CecilTokenResolver(IAssemblyResolver assemblyResolver)
        {
            this.assemblyResolver = assemblyResolver;
        }

        #region ITokenResolver Members

        public object Resolve(TokenReference token)
        {
            var assembly = assemblyResolver.Resolve(AssemblyNameReference.Parse(token.AssemblyName));
            if(assembly != null)
            {
                return assembly.MainModule.LookupToken(token.Token);
            }

            return null;
        }

        public T Resolve<T>(TokenReference token)
            where T : class
        {
            return Resolve(token) as T;
        }

        #endregion
    }
}
